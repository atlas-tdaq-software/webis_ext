#!/bin/bash

TDAQ_RELEASE=tdaq-05-05-00

LOGDIR=/logs/$TDAQ_RELEASE/web_is_ext
mkdir -p $LOGDIR
rc=$?
if [[ $rc != 0 ]] ; then
    echo "Can't create log directory '" $LOGDIR "'"
    exit $rc
fi

LOGFILE=$LOGDIR/log.web_is_ext.$$

touch $LOGFILE
rc=$?
if [[ $rc != 0 ]] ; then
    echo "Can't create log file '" $LOGFILE "'"
    exit $rc
fi

if [ -f /det/tdaq/scripts/setup_TDAQ_$TDAQ_RELEASE.sh ]; then
    source /det/tdaq/scripts/setup_TDAQ_$TDAQ_RELEASE.sh
else
    echo "TDAQ release setup script is not found"
    exit 2
fi

export PYTHONPATH=/det/tdaq/webis_server:$PYTHONPATH
export PATH=/det/tdaq/webis_ext:$PATH

trap terminate SIGINT SIGTERM

function terminate() {
    pkill -TERM -P $$
    exit 0
}

unset DISPLAY
while true
do
    webis_ext_server.py &>> ${LOGFILE} &
    wait $!
    sleep 1
done

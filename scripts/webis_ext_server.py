#!/usr/bin/env tdaq_python

from twisted.web import resource, server, http
from twisted.internet import reactor
from webis_server import toXML
import time
import thread
import ispy
import ipc
import ers
import sys
import uuid
import argparse
import os

ispy.OWLDate.__str__ = ispy.OWLDate.str

release=os.environ['TDAQ_VERSION']

def getArg(request, key, isRequired=True):
    value = request.args.has_key(key) and request.args[key][0] or None
    if (value == None and isRequired):
	request.write( "event: service-error\ndata: Invalid request '" + request.uri + " : '"
	    + key + "' argument is not provided\n\n")
    return value

def doPing(server, dummy):
    "used by the ping thread to ping clients"
    while 1:
	for s in server.services.values():
	    s.ping()
	    time.sleep(10)
    	
class ISReceiver(ispy.ISInfoReceiver):
    def __init__(self, service, partition, name, criteria, key):
	ispy.ISInfoReceiver.__init__(self, ispy.IPCPartition(partition))
	self.sessions = []
	self.service = service
        self.partition = partition
	self.name = name
	self.criteria = criteria
        self.key = key
	if(self.criteria == None):
	    self.server = self.name.split('.')[0]
            self.subscribe_info(self.name, self.callback)
	else:
	    self.server = self.name
            self.subscribe_info(self.name, self.callback, ispy.ISCriteria(self.criteria))

    def addSession(self, s):
	if not s in self.sessions:
	    self.sessions.append(s)
	s.addReceiver(self)

    def removeSession(self, s):
	if s in self.sessions:
	    self.sessions.remove(s)
        s.removeReceiver(self)
	
	ers.log("ISReceiver has " + str(len(self.sessions)) + " session(s)")
        if len(self.sessions) == 0:
            self.service.removeReceiver(self)
            self.removeSubscriptions()

    def callback(self, info):
        try:
	    any = ispy.ISInfoDynAny()
	    info.value(any)
	    content = toXML(info.name(), any, None)
	    begin = '''<?xml version="1.0" encoding="UTF-8"?>
		    <objects release="%s" partition="%s" server="%s" tag="%s">
		    ''' % (release, self.partition, self.server, info.tag())
	    end = '''</objects>'''
	    data = begin + content + end
	    for c in self.sessions:
                c.sendMsg('is-callback', data.replace('\n', ''), [self.key, str(info.reason())])
	except Exception as ex:
	    ers.log(ex)

    def removeSubscriptions(self):
        try:
	    if(self.criteria == None):
		self.unsubscribe(self.name, True)
	    else:
		self.unsubscribe(self.name, ispy.ISCriteria(self.criteria), True)
	except Exception as ex:
            ers.log(ex)
            		
class SSESession():
    def __init__(self, service, request):
	self.receivers = []
        self.service = service 
        self.request = request
	self.clientIP = request.getClientIP()
	self.sessionID = request.args['sid'][0]

	#set callbacks to stop when the session closes the connection
	deferred = request.notifyFinish()
	deferred.addCallback(self.stop)
	deferred.addErrback(self.stop)
	        
	self.request.setHeader('Connection','keep-alive')
	self.request.setHeader('Content-Type','text/event-stream')
	self.request.setHeader('Cache-Control','no-cache, must-revalidate')
	self.request.setHeader('Expires','Mon, 26 Jul 1997 05:00:00 GMT')
        
	self.service.addSession(self)
        self.sendMsg('session-created', self.sessionID)
	ers.log( "New '" + self.sessionID + "' SSE session has been created on request of the " 
		+ self.clientIP + " client")

    def addReceiver(self, r):
    	self.receivers.append(r);

    def removeReceiver(self, r):
    	if r in self.receivers:
            self.receivers.remove(r);

    def sendMsg(self, type, data, params = []):
	"send a message to the session"
        d = ''.join(p + ',' for p in params) + str(data)
	self.request.write("event: " + type + "\ndata: " + d + "\n\n")
	self.lastMessageTime = time.time()
        ers.debug(1, '"event: ' + type + ' data: ' + d + '" message has been sent')

    def ping(self):
	if (time.time() - self.lastMessageTime) > 30:
	    self.sendMsg('ping', self.sessionID)
                
    def stop(self, reason):
	"session closed"
	ers.log( "The '" + self.sessionID + "' SSE session for the "
		+ self.clientIP + " client has been closed")
	ers.log( "Session contains " + str(len(self.receivers)) + " receivers" )
	for r in self.receivers:
	    r.removeSession(self)
	self.service.removeSession(self)
            
class WebISExtService():
    def __init__(self):
    	self.name = 'WebISExt'
        self.receivers = {}
        self.sessions = []
	    
    def ping(self):
        for s in self.sessions:
	    s.ping()
        
    def parse(self, request):
	ers.log("Received request: " + request.uri)
                
        if getArg(request, "sid") == None:
	    return
	    
	session = self.findSession(request)
	
        RET = ''
	if session == None:
	    session = SSESession(self, request)
            RET = server.NOT_DONE_YET

        command = getArg(request, "cmd", False)
	if command == None:
	    return RET

        partition = getArg(request, "partition")
        name = getArg(request, "name")
        
	if partition == None or name == None:
	    return RET

        criteria = getArg(request, "criteria", False)
	                  
	key = request.uri[request.uri.find('&') + 1 : request.uri.rfind('&')]
	if command == "subscribe":
            if not key in self.receivers:
		try:
		    self.receivers[key] = ISReceiver(self, partition, name, criteria, key)
		except Exception as ex:
		    session.sendMsg('service-error', str(ex), [key])
		    return RET
	    
            self.receivers[key].addSession(session)
	    session.sendMsg('subscribed', key)
                        
	elif command == "unsubscribe":
	    if key in self.receivers:
		try:
		    self.receivers[key].removeSession(session)
		    session.sendMsg('unsubscribed', key)
		except Exception as ex:
		    session.sendMsg('service-error', str(ex), [key])
	    else:
		session.sendMsg('service-error', "Subscription does not exist", [key])
	else:
	    session.sendMsg('service-error', "Invalid request '"
			    + request.uri + "' : unknown '"
			    + command + "' command is provided", [key])
	return RET
		
    def removeSession(self, s):
        if s in self.sessions:
	    self.sessions.remove(s)
        
    def removeReceiver(self, r):
    	try:
            del self.receivers[r.key]
	except KeyError:
	    pass

    def findSession(self, request):
	"Can be used to find the SSE session of the client, if available"
	for s in self.sessions:
	    if request.args.has_key('sid'):
		if request.args['sid'][0] == s.sessionID and request.getClientIP() == s.clientIP:
		    return s
	return None
        
    def addSession(self, session):
        self.sessions.append(session)
		
class SSEServer(resource.Resource):
    "The actual server handling get requests etc."
    isLeaf = True
    services = {}

    def __init__(self, services, pingUsers):
	resource.Resource.__init__(self)
	self.services = services
        
	if pingUsers:
	    thread.start_new_thread(doPing, (self,None))
	    ers.log("Pinger thread is started")

    def render_GET(self, request):
	"Handle HTTP GET requests"

	path = request.path
	if len(path) > 1:
	    path = request.path.rstrip('/')
            
	if self.services.has_key(path):
	    ers.log("Service received request to: " + self.services[path].name)
	    return self.services[path].parse(request)
	else:
	    ers.log("Request to unknown service has been received :" + path)
            
	return ''

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Start extended Web IS service.')
    parser.add_argument('-p', '--port', type=int, default=3080,
		       help='port to listen')
    parser.add_argument('-n', '--no_keep_alive', action='store_true',
		       help='disable pinging for keeping connections open')

    args = parser.parse_args()
    
    sse = SSEServer({'/is': WebISExtService()}, not args.no_keep_alive)

    reactor.listenTCP(args.port, server.Site(sse))
    ers.log('SSE Server has been started for the "' + release + '" tdaq release on port ' + str(args.port))
    reactor.run()

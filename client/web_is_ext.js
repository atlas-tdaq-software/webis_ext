var sessionId=null;
var eventSource=null;
var subscribers=[];
var baseUrl='/web_is_ext/is?';

function uuid()
{
    window.onbeforeunload = function() {
	console.log("Page is being refreshed, closing the Web IS Ext connection...");
        sendToAll('cmd=unsubscribe');
        eventSource.close();
    };

    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, 
    	function(c) {
	    var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
	    return v.toString(16);
	});
}

Array.prototype.findObject = function(key) 
{
    for(var i = 0, l = this.length; i < l; i++) 
    	if (this[i].params == key) 
            return this[i];
    return undefined;
}

Array.prototype.removeObject = function(key) 
{
    for(var i = 0, l = this.length; i < l; i++) 
    	if (this[i].params == key)
            return this.splice(i,1)[0];
    return undefined;
}

function defaultErrorCallback(msg)
{
    console.log(msg);
}

function sendXMLHttpRequest(request)
{
    var xmlhttp=null;
    if (window.XMLHttpRequest) {
    	// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    }
    else {
    	// code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    
    r = baseUrl + request + (sessionId != null ? '&sid=' + sessionId : "");
    xmlhttp.open("GET", r, false);
    xmlhttp.send();
}

function sendRequest(command, request) 
{
    if(sessionId==null)
    	openSSESession(request);
    else
    	sendXMLHttpRequest(command + '&' + request.params);
}

function sendToAll(command) 
{
    for(var i = 0, l = subscribers.length; i < l; i++) {
        if (subscribers[i].isSubscribed)
            sendRequest(command, subscribers);
    }
}

function errorToAll(msg) 
{
    var errorCb = null;
    for(var i = 0, l = subscribers.length; i < l; i++) {
	if (errorCb != subscribers[i].errorCallback) {
            errorCb = subscribers[i].errorCallback;
            errorCb(msg);
        }
    }
}

function openSSESession(request) 
{
    if(sessionId!=null)
        return;
    
    sessionId = uuid()
    eventSource = new EventSource(baseUrl + "&sid=" + sessionId);
    
    eventSource.onerror = function(e) { 
	errorToAll('Connection to the Extended Web IS service has failed : ' + e.data);
    };

    eventSource.addEventListener('session-created', 
    	function(e) { 
	    console.log('New session has been created');
	    sendRequest('cmd=subscribe', request);
	}, false);

    eventSource.addEventListener('subscribed', 
    	function(e) { 
	    console.log("'" + e.data +"' subcription request was successful");
	    subscription = subscribers.findObject(e.data);
	    if (subscription != undefined)
	    	subscription.isSubscribed = true;
	}, false);
	 
    eventSource.addEventListener('unsubscribed', 
    	function(e) { 
	    console.log("'" + e.data +"' unsubcription request was successful");
	}, false);
	 
    eventSource.addEventListener('service-error', 
    	function(e) { 
            var obj = e.data.split(',', 2);
            if(obj.length != 2){
		console.log("Error received from the remote service: " + e.data)
            }
	    var s = subscribers.findObject(obj[0]);
	    if(s != undefined){
		s.errorCallback(obj[1]);
	    }
            else {
		console.log("Error received from the remote service: " + e.data)
            }
	}, false);
	 	
    eventSource.addEventListener('is-callback', 
    	function(e) {
            var obj = e.data.split(',', 3);
            if(obj.length != 3){
		console.log("Callback data has invalid format: " + e.data)
            }
            else{
		var s = subscribers.findObject(obj[0]);
		if(s != undefined){
		    s.callback(obj[2], obj[1]);
		}
		else {
		    console.log("Callback '" + obj[0] + "' is not found");
		}
	    }
	}, false);
}

function createParams(partition, is_name, criteria)
{
    return "partition=" + partition + "&name=" + is_name + (criteria != undefined ? "&criteria=" + criteria : "")
}

function subscribe(partition, is_name, criteria, callBackFunction, errorFunction)
{
    var subscription = {
	params:		    createParams(partition, is_name, criteria),
	callback:	    callBackFunction,
	errorCallback:	    errorFunction != undefined ? errorFunction : defaultErrorCallback,
	isSubscribed:	    false
    }

    if(subscribers.findObject(subscription.params) != undefined) {
	subscription.errorCallback("'" + subscription.params + "' subscription already exist")
	return;
    }

    subscribers.push(subscription);
    sendRequest("cmd=subscribe", subscription);
}

var web_is_ext = {
    subscribe_all : function(partition, is_name, criteria, callBackFunction, errorFunction)
    {
    	subscribe(partition, is_name, criteria, callBackFunction, errorFunction)
    },

    subscribe_one : function(partition, is_name, callBackFunction, errorFunction)
    {
    	subscribe(partition, is_name, null, callBackFunction, errorFunction)
    },

    unsubscribe : function(partition, is_name, criteria)
    {
	params = createParams(partition, is_name, criteria);	    
        
	var s = subscribers.removeObject(params);
	if (s == undefined) {
            console.log("'" + params + "' subscription does not exist");
            return;
        }
        
        if (!s.isSubscribed) {
            s.errorCallback("'" + params + "' subscription does not exist");
            return;
        }
            
	sendRequest("cmd=unsubscribe", s);
    },
}
